'use strict';

angular.module('mantenimientoApp.admin', [
  'mantenimientoApp.auth',
  'ui.router'
]);
