(function(){

  'use strict';

  angular.module('mantenimientoApp')
    .controller('EmpresasListCtrl', EmpresasListCtrl);

    EmpresasListCtrl.$inject=['EmpresasServices'];

    function EmpresasListCtrl(EmpresasServices){
      var vm = this;
      vm.empresas= EmpresasServices.query();
    }

})();
