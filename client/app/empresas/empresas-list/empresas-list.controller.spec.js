'use strict';

describe('Controller: EmpresasListCtrl', function () {

  // load the controller's module
  beforeEach(module('mantenimientoApp'));

  var EmpresasListCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EmpresasListCtrl = $controller('EmpresasListCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
