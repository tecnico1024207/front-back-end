(function(){
  'use strict';

  angular.module('mantenimientoApp')
    .directive('empresasList', empresasList);


        function empresasList(){
          return {
            templateUrl: 'app/empresas/empresas-list/empresas-list.html',
            restrict: 'EA',
            controller: 'EmpresasListCtrl',
            controllerAs: 'vm'
          };
        }

})();
