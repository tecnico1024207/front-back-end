'use strict';

describe('Directive: empresasList', function () {

  // load the directive's module and view
  beforeEach(module('mantenimientoApp'));
  beforeEach(module('app/empresas/empresas-list/empresas-list.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<empresas-list></empresas-list>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the empresasList directive');
  }));
});
