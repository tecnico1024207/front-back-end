(function(){
  'use strict';

  angular.module('mantenimientoApp')
    .config(function ($stateProvider) {
      $stateProvider
        .state('empresas', {
          url: '/empresas',
          template: '<empresas-list></empresas-list>'
        });
    });
})();
