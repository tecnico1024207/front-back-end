(function(){

  'use strict';

  angular.module('mantenimientoApp')
    .factory('EmpresasServices', EmpresasServices);
    EmpresasServices.$inject=['$resource'];
    function EmpresasServices($resource){
      return $resource('/api/empresas/:idEmpresa',{
        idEmpresa: '@idEmpresa'
      },
      {
            'update':{
              method: 'PUT'
            }
      });
    }

})();
