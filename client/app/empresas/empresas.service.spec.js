'use strict';

describe('Service: empresas', function () {

  // load the service's module
  beforeEach(module('mantenimientoApp'));

  // instantiate service
  var empresas;
  beforeEach(inject(function (_empresas_) {
    empresas = _empresas_;
  }));

  it('should do something', function () {
    expect(!!empresas).toBe(true);
  });

});
