(function(){
'use strict';

angular.module('mantenimientoApp')
  .factory('TiposEquiposService', TiposEquiposService);

TiposEquiposService.$inject = ['$resource'];
  function TiposEquiposService($resource) {
    return $resource('/api/tiposEquipos/:id', {
      id: '@id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
