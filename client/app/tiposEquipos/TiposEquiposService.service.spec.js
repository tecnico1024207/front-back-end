'use strict';

describe('Service: TiposEquiposService', function () {

  // load the service's module
  beforeEach(module('mantenimientoApp'));

  // instantiate service
  var TiposEquiposService;
  beforeEach(inject(function (_TiposEquiposService_) {
    TiposEquiposService = _TiposEquiposService_;
  }));

  it('should do something', function () {
    expect(!!TiposEquiposService).toBe(true);
  });

});
