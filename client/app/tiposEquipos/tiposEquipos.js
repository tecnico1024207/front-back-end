'use strict';

angular.module('mantenimientoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('tiposEquipos', {
        url: '/tiposEquipos',
        template: '<tipos-equipos-list></tipos-equipos-list>'
      })
      .state('tiposEquiposCreate', {
        url: '/tiposEquiposCreate',
        template: '<tipos-equipos-create></tipos-equipos-create>'
      }).
      state('tiposEquiposEdit', {
        url: '/tiposEquiposEdit/:id',
        template: '<tipos-equipos-edit></tipos-equipos-edit>'
      });
  });
