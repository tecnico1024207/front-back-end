(function() {
  'use strict';

  angular.module('mantenimientoApp')
    .controller('TiposEquiposCreateCtrl', TiposEquiposCreateCtrl);

  TiposEquiposCreateCtrl.$inject = ['TiposEquiposService', '$location'];

  function TiposEquiposCreateCtrl(TiposEquiposService, $location) {
    var vm = this;

    vm.create = create;

    function create() {
      TiposEquiposService.save(vm.selected, function() {
        $location.path('/tiposEquipos');
      });
    }

  }
})();
