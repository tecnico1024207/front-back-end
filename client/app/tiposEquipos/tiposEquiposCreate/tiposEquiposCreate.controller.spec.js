'use strict';

describe('Controller: TiposEquiposCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('mantenimientoApp'));

  var TiposEquiposCreateCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TiposEquiposCreateCtrl = $controller('TiposEquiposCreateCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
