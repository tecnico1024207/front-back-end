'use strict';

angular.module('mantenimientoApp')
  .directive('tiposEquiposCreate', function () {
    return {
      templateUrl: 'app/tiposEquipos/tiposEquiposCreate/tiposEquiposCreate.html',
      restrict: 'EA',
      controller: 'TiposEquiposCreateCtrl',
      controllerAs: 'vm'
    };
  });
