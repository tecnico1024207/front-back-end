'use strict';

describe('Directive: tiposEquiposCreate', function () {

  // load the directive's module and view
  beforeEach(module('mantenimientoApp'));
  beforeEach(module('app/tiposEquipos/tiposEquiposCreate/tiposEquiposCreate.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<tipos-equipos-create></tipos-equipos-create>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the tiposEquiposCreate directive');
  }));
});
