(function() {

  'use strict';

  angular.module('mantenimientoApp')
    .controller('TiposEquiposEditCtrl', TiposEquiposEditCtrl);

  TiposEquiposEditCtrl.$inject = ['TiposEquiposService', '$location', '$stateParams', '$mdToast'];

  function TiposEquiposEditCtrl(TiposEquiposService, $location, $stateParams, $mdToast) {
    var vm = this;

    vm.update = update;

    vm.selected = TiposEquiposService.get({
      id: $stateParams.id
    });

    function update() {
      TiposEquiposService.update({
        id: $stateParams.id
      }, vm.selected).$promise.then(function() {
        $location.path('/tiposEquipos');
        $mdToast.show(
          $mdToast.simple()
          .textContent('Tipo equipos actualizado exitosamente')
          .position('top right'));
      });
    }

  }
})();
