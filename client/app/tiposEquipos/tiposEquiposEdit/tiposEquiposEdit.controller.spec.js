'use strict';

describe('Controller: TiposEquiposEditCtrl', function () {

  // load the controller's module
  beforeEach(module('mantenimientoApp'));

  var TiposEquiposEditCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TiposEquiposEditCtrl = $controller('TiposEquiposEditCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
