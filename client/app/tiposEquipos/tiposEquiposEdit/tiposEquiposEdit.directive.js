(function() {
  'use strict';

  angular.module('mantenimientoApp')
    .directive('tiposEquiposEdit', tiposEquiposEdit);

  function tiposEquiposEdit() {
    return {
      controller: 'TiposEquiposEditCtrl',
      controllerAs: 'vm',
      templateUrl: 'app/tiposEquipos/tiposEquiposEdit/tiposEquiposEdit.html',
      restrict: 'EA'
    };
  }
})();
