'use strict';

describe('Directive: tiposEquiposEdit', function () {

  // load the directive's module and view
  beforeEach(module('mantenimientoApp'));
  beforeEach(module('app/tiposEquipos/tiposEquiposEdit/tiposEquiposEdit.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<tipos-equipos-edit></tipos-equipos-edit>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the tiposEquiposEdit directive');
  }));
});
