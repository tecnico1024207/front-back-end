(function(){
'use strict';

angular.module('mantenimientoApp')
  .controller('TiposEquiposListCtrl', TiposEquiposListCtrl);

  TiposEquiposListCtrl.$inject = ['TiposEquiposService', '$mdDialog', '$mdToast'];
  function TiposEquiposListCtrl(TiposEquiposService, $mdDialog, $mdToast) {
    var vm = this;
    vm.query = {
      limit: 5,
      page: 1
    };
    vm.showConfirm = showConfirm;

    vm.tiposEquipos = TiposEquiposService.query();

    function showConfirm(ev, item) {
      var confirm = $mdDialog.confirm()
        .title('Eliminar')
        .textContent('Esta seguro que desea eliminar este item?')
        .targetEvent(ev)
        .ok('Si')
        .cancel('No');
      $mdDialog.show(confirm).then(function() {
        TiposEquiposService.delete({id: item.idTipoEquipo})
         .$promise.then(function(){
           vm.tiposEquipos = TiposEquiposService.query();
           $mdToast.show(
             $mdToast.simple()
             .textContent('Item eliminado exitosamente')
             .position('bottom right'));
         });
      });
    }
  }

})();
