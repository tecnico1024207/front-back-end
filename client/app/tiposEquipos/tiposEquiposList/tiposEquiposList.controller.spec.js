'use strict';

describe('Controller: TiposEquiposListCtrl', function () {

  // load the controller's module
  beforeEach(module('mantenimientoApp'));

  var TiposEquiposListCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TiposEquiposListCtrl = $controller('TiposEquiposListCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
