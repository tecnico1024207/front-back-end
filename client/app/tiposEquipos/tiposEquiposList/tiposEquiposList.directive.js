'use strict';

angular.module('mantenimientoApp')
  .directive('tiposEquiposList', function () {
    return {
      templateUrl: 'app/tiposEquipos/tiposEquiposList/tiposEquiposList.html',
      restrict: 'EA',
      controller: 'TiposEquiposListCtrl',
      controllerAs: 'vm'
    };
  });
