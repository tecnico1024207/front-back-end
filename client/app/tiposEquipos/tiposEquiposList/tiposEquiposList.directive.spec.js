'use strict';

describe('Directive: tiposEquiposList', function () {

  // load the directive's module and view
  beforeEach(module('mantenimientoApp'));
  beforeEach(module('app/tiposEquipos/tiposEquiposList/tiposEquiposList.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<tipos-equipos-list></tipos-equipos-list>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the tiposEquiposList directive');
  }));
});
