'use strict';

angular.module('mantenimientoApp.auth', [
  'mantenimientoApp.constants',
  'mantenimientoApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
