'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
    'title': 'Home',
    'state': 'main'
  }];

  isCollapsed = true;
  //end-non-standard
  
  this.getURL = function (){
      console.log($location.path);
      return $location.path();
  }

  constructor(Auth) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.isUser = Auth.isUser;
    this.getCurrentUser = Auth.getCurrentUser;
  }
}

angular.module('mantenimientoApp')
  .controller('NavbarController', NavbarController);
