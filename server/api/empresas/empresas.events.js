/**
 * Empresas model events
 */

'use strict';

import {EventEmitter} from 'events';
var Empresas = require('../../sqldb').Empresas;
var EmpresasEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
EmpresasEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Empresas.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    EmpresasEvents.emit(event + ':' + doc._id, doc);
    EmpresasEvents.emit(event, doc);
    done(null);
  }
}

export default EmpresasEvents;
