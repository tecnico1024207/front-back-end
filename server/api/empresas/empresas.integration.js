'use strict';

var app = require('../..');
import request from 'supertest';

var newEmpresas;

describe('Empresas API:', function() {

  describe('GET /api/empresas', function() {
    var empresass;

    beforeEach(function(done) {
      request(app)
        .get('/api/empresas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          empresass = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      empresass.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/empresas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/empresas')
        .send({
          name: 'New Empresas',
          info: 'This is the brand new empresas!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newEmpresas = res.body;
          done();
        });
    });

    it('should respond with the newly created empresas', function() {
      newEmpresas.name.should.equal('New Empresas');
      newEmpresas.info.should.equal('This is the brand new empresas!!!');
    });

  });

  describe('GET /api/empresas/:id', function() {
    var empresas;

    beforeEach(function(done) {
      request(app)
        .get('/api/empresas/' + newEmpresas._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          empresas = res.body;
          done();
        });
    });

    afterEach(function() {
      empresas = {};
    });

    it('should respond with the requested empresas', function() {
      empresas.name.should.equal('New Empresas');
      empresas.info.should.equal('This is the brand new empresas!!!');
    });

  });

  describe('PUT /api/empresas/:id', function() {
    var updatedEmpresas;

    beforeEach(function(done) {
      request(app)
        .put('/api/empresas/' + newEmpresas._id)
        .send({
          name: 'Updated Empresas',
          info: 'This is the updated empresas!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedEmpresas = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedEmpresas = {};
    });

    it('should respond with the updated empresas', function() {
      updatedEmpresas.name.should.equal('Updated Empresas');
      updatedEmpresas.info.should.equal('This is the updated empresas!!!');
    });

  });

  describe('DELETE /api/empresas/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/empresas/' + newEmpresas._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when empresas does not exist', function(done) {
      request(app)
        .delete('/api/empresas/' + newEmpresas._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
