'use strict';

export default function(sequelize, DataTypes) {
  var Empresas= sequelize.define('empresas', {
    idEmpresa: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field:'id_empresa'
    },
    nombre: {
      type: DataTypes.STRING,
      field: 'nombre',
      allowNull: false
    }
  },{
    classMethods:{
      associate:function(models){
        Empresas.hasMany(models.Sedes,{
          foreignKey:{
            name:'idEmpresa',
            field: 'id_empresa',
            allowNull: false
          }
        });
      }
    }
  },
  {
   underscored: true
  });
  return Empresas;
}
