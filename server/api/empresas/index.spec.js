'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var empresasCtrlStub = {
  index: 'empresasCtrl.index',
  show: 'empresasCtrl.show',
  create: 'empresasCtrl.create',
  update: 'empresasCtrl.update',
  destroy: 'empresasCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var empresasIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './empresas.controller': empresasCtrlStub
});

describe('Empresas API Router:', function() {

  it('should return an express router instance', function() {
    empresasIndex.should.equal(routerStub);
  });

  describe('GET /api/empresas', function() {

    it('should route to empresas.controller.index', function() {
      routerStub.get
        .withArgs('/', 'empresasCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/empresas/:id', function() {

    it('should route to empresas.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'empresasCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/empresas', function() {

    it('should route to empresas.controller.create', function() {
      routerStub.post
        .withArgs('/', 'empresasCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/empresas/:id', function() {

    it('should route to empresas.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'empresasCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/empresas/:id', function() {

    it('should route to empresas.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'empresasCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/empresas/:id', function() {

    it('should route to empresas.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'empresasCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
