/**
 * Equipos model events
 */

'use strict';

import {EventEmitter} from 'events';
var Equipos = require('../../sqldb').Equipos;
var EquiposEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
EquiposEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Equipos.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    EquiposEvents.emit(event + ':' + doc._id, doc);
    EquiposEvents.emit(event, doc);
    done(null);
  }
}

export default EquiposEvents;
