'use strict';

var app = require('../..');
import request from 'supertest';

var newEquipos;

describe('Equipos API:', function() {

  describe('GET /api/equipos', function() {
    var equiposs;

    beforeEach(function(done) {
      request(app)
        .get('/api/equipos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          equiposs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      equiposs.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/equipos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/equipos')
        .send({
          name: 'New Equipos',
          info: 'This is the brand new equipos!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newEquipos = res.body;
          done();
        });
    });

    it('should respond with the newly created equipos', function() {
      newEquipos.name.should.equal('New Equipos');
      newEquipos.info.should.equal('This is the brand new equipos!!!');
    });

  });

  describe('GET /api/equipos/:id', function() {
    var equipos;

    beforeEach(function(done) {
      request(app)
        .get('/api/equipos/' + newEquipos._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          equipos = res.body;
          done();
        });
    });

    afterEach(function() {
      equipos = {};
    });

    it('should respond with the requested equipos', function() {
      equipos.name.should.equal('New Equipos');
      equipos.info.should.equal('This is the brand new equipos!!!');
    });

  });

  describe('PUT /api/equipos/:id', function() {
    var updatedEquipos;

    beforeEach(function(done) {
      request(app)
        .put('/api/equipos/' + newEquipos._id)
        .send({
          name: 'Updated Equipos',
          info: 'This is the updated equipos!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedEquipos = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedEquipos = {};
    });

    it('should respond with the updated equipos', function() {
      updatedEquipos.name.should.equal('Updated Equipos');
      updatedEquipos.info.should.equal('This is the updated equipos!!!');
    });

  });

  describe('DELETE /api/equipos/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/equipos/' + newEquipos._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when equipos does not exist', function(done) {
      request(app)
        .delete('/api/equipos/' + newEquipos._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
