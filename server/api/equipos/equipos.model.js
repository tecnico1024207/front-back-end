'use strict';

export default function(sequelize, DataTypes) {
    return sequelize.define('equipos', {
        id_equipo: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        serial: {
            type: DataTypes.STRING,
            allowNull: true
        },
        codigo_barras: {
            type: DataTypes.STRING,
            allowNull: true
        },
        referencia: {
            type: DataTypes.STRING,
            allowNull: true
        },
        id_cliente: {
            type: DataTypes.STRING,
            allowNull: false
        },
        id_tipo_equipo: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'tipos_equipos',
                key: 'id_tipo_equipo'
            }
        },
        id_marca: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'marcas',
                key: 'id_marca'
            }
        },
        id_usuario: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'usuarios',
                key: 'id_usuario'
            }
        }
    }, {
        tableName: 'equipos'
    });
}
