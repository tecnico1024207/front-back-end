'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var equiposCtrlStub = {
  index: 'equiposCtrl.index',
  show: 'equiposCtrl.show',
  create: 'equiposCtrl.create',
  update: 'equiposCtrl.update',
  destroy: 'equiposCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var equiposIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './equipos.controller': equiposCtrlStub
});

describe('Equipos API Router:', function() {

  it('should return an express router instance', function() {
    equiposIndex.should.equal(routerStub);
  });

  describe('GET /api/equipos', function() {

    it('should route to equipos.controller.index', function() {
      routerStub.get
        .withArgs('/', 'equiposCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/equipos/:id', function() {

    it('should route to equipos.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'equiposCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/equipos', function() {

    it('should route to equipos.controller.create', function() {
      routerStub.post
        .withArgs('/', 'equiposCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/equipos/:id', function() {

    it('should route to equipos.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'equiposCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/equipos/:id', function() {

    it('should route to equipos.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'equiposCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/equipos/:id', function() {

    it('should route to equipos.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'equiposCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
