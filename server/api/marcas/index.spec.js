'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var marcasCtrlStub = {
  index: 'marcasCtrl.index',
  show: 'marcasCtrl.show',
  create: 'marcasCtrl.create',
  update: 'marcasCtrl.update',
  destroy: 'marcasCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var marcasIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './marcas.controller': marcasCtrlStub
});

describe('Marcas API Router:', function() {

  it('should return an express router instance', function() {
    marcasIndex.should.equal(routerStub);
  });

  describe('GET /sever/api/marcas', function() {

    it('should route to marcas.controller.index', function() {
      routerStub.get
        .withArgs('/', 'marcasCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /sever/api/marcas/:id', function() {

    it('should route to marcas.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'marcasCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /sever/api/marcas', function() {

    it('should route to marcas.controller.create', function() {
      routerStub.post
        .withArgs('/', 'marcasCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /sever/api/marcas/:id', function() {

    it('should route to marcas.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'marcasCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /sever/api/marcas/:id', function() {

    it('should route to marcas.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'marcasCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /sever/api/marcas/:id', function() {

    it('should route to marcas.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'marcasCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
