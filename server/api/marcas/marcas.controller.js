/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sever/api/marcas              ->  index
 * POST    /sever/api/marcas              ->  create
 * GET     /sever/api/marcas/:id          ->  show
 * PUT     /sever/api/marcas/:id          ->  update
 * DELETE  /sever/api/marcas/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {Marcas} from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Marcass
export function index(req, res) {
  return Marcas.findAll()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Marcas from the DB
export function show(req, res) {
  return Marcas.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Marcas in the DB
export function create(req, res) {
  return Marcas.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Marcas in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Marcas.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Marcas from the DB
export function destroy(req, res) {
  return Marcas.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
