/**
 * Marcas model events
 */

'use strict';

import {EventEmitter} from 'events';
var Marcas = require('../../sqldb').Marcas;
var MarcasEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MarcasEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Marcas.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    MarcasEvents.emit(event + ':' + doc._id, doc);
    MarcasEvents.emit(event, doc);
    done(null);
  }
}

export default MarcasEvents;
