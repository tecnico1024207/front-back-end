'use strict';

var app = require('../..');
import request from 'supertest';

var newMarcas;

describe('Marcas API:', function() {

  describe('GET /sever/api/marcas', function() {
    var marcass;

    beforeEach(function(done) {
      request(app)
        .get('/sever/api/marcas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          marcass = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      marcass.should.be.instanceOf(Array);
    });

  });

  describe('POST /sever/api/marcas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/sever/api/marcas')
        .send({
          name: 'New Marcas',
          info: 'This is the brand new marcas!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMarcas = res.body;
          done();
        });
    });

    it('should respond with the newly created marcas', function() {
      newMarcas.name.should.equal('New Marcas');
      newMarcas.info.should.equal('This is the brand new marcas!!!');
    });

  });

  describe('GET /sever/api/marcas/:id', function() {
    var marcas;

    beforeEach(function(done) {
      request(app)
        .get('/sever/api/marcas/' + newMarcas._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          marcas = res.body;
          done();
        });
    });

    afterEach(function() {
      marcas = {};
    });

    it('should respond with the requested marcas', function() {
      marcas.name.should.equal('New Marcas');
      marcas.info.should.equal('This is the brand new marcas!!!');
    });

  });

  describe('PUT /sever/api/marcas/:id', function() {
    var updatedMarcas;

    beforeEach(function(done) {
      request(app)
        .put('/sever/api/marcas/' + newMarcas._id)
        .send({
          name: 'Updated Marcas',
          info: 'This is the updated marcas!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMarcas = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMarcas = {};
    });

    it('should respond with the updated marcas', function() {
      updatedMarcas.name.should.equal('Updated Marcas');
      updatedMarcas.info.should.equal('This is the updated marcas!!!');
    });

  });

  describe('DELETE /sever/api/marcas/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/sever/api/marcas/' + newMarcas._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when marcas does not exist', function(done) {
      request(app)
        .delete('/sever/api/marcas/' + newMarcas._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
