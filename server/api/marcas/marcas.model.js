'use strict';

export default function(sequelize, DataTypes) {
    return sequelize.define('marcas', {
      id_marca: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true
      },
      descripcion: {
        type: DataTypes.STRING,
        allowNull: false
      }
    }, {
      tableName: 'marcas'
    });
}
