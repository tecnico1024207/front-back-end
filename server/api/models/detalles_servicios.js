/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('detalles_servicios', {
    id_servicio_producto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'servicios_productos',
        key: 'id_sevicio_producto'
      }
    },
    id_orden_servicio: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'ordenes_servicios',
        key: 'id_orden_servicio'
      }
    },
    cantidad: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'detalles_servicios'
  });
};
