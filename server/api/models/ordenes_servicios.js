/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ordenes_servicios', {
    id_orden_servicio: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false
    },
    solucion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    diagnostico: {
      type: DataTypes.STRING,
      allowNull: false
    },
    falla_cliente: {
      type: DataTypes.STRING,
      allowNull: false
    },
    valor: {
      type: 'DOUBLE',
      allowNull: true
    },
    id_empleado: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'usuarios',
        key: 'id_usuario'
      }
    },
    id_equipo: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'equipos',
        key: 'id_equipo'
      }
    }
  }, {
    tableName: 'ordenes_servicios'
  });
};
