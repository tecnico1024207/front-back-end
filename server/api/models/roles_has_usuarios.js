/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('roles_has_usuarios', {
    id_rol: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'roles',
        key: 'id_rol'
      }
    },
    id_usuario: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'usuarios',
        key: 'id_usuario'
      }
    }
  }, {
    tableName: 'roles_has_usuarios'
  });
};
