/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sedes', {
    id_sede: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: true
    },
    telefono: {
      type: DataTypes.STRING,
      allowNull: true
    },
    direccion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_empresa: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'empresas',
        key: 'id_empresa'
      }
    }
  }, {
    tableName: 'sedes'
  });
};
