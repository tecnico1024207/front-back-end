/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('servicios_productos', {
    id_sevicio_producto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false
    },
    valor: {
      type: 'DOUBLE',
      allowNull: true
    },
    referencia: {
      type: DataTypes.STRING,
      allowNull: false
    },
    unidad_medida: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'servicios_productos'
  });
};
