'use strict';

export default function(sequelize, DataTypes) {
    var Roles = sequelize.define('roles', {
      idRol: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        field: 'id_rol'
      },
      nombre: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
        classMethods: {
            associate: function(models){
                Roles.belongsToMany(models.Usuarios,{
                    through: 'roles_has_usuarios',
                    foreignKey: {
                        name:'idRol',
                        field: 'id_rol'
                    },
                    otherKey: {
                        name: 'idUsuario',
                        field: 'id_usuario'
                    }
                });
            }
        }
    },
    {
      underscored: true
    });

    return Roles;
  }
