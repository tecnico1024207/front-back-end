'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var roleshasusuariosCtrlStub = {
  index: 'roleshasusuariosCtrl.index',
  show: 'roleshasusuariosCtrl.show',
  create: 'roleshasusuariosCtrl.create',
  update: 'roleshasusuariosCtrl.update',
  destroy: 'roleshasusuariosCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var roleshasusuariosIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './roleshasusuarios.controller': roleshasusuariosCtrlStub
});

describe('Roleshasusuarios API Router:', function() {

  it('should return an express router instance', function() {
    roleshasusuariosIndex.should.equal(routerStub);
  });

  describe('GET /api/roleshasusuarios', function() {

    it('should route to roleshasusuarios.controller.index', function() {
      routerStub.get
        .withArgs('/', 'roleshasusuariosCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/roleshasusuarios/:id', function() {

    it('should route to roleshasusuarios.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'roleshasusuariosCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/roleshasusuarios', function() {

    it('should route to roleshasusuarios.controller.create', function() {
      routerStub.post
        .withArgs('/', 'roleshasusuariosCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/roleshasusuarios/:id', function() {

    it('should route to roleshasusuarios.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'roleshasusuariosCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/roleshasusuarios/:id', function() {

    it('should route to roleshasusuarios.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'roleshasusuariosCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/roleshasusuarios/:id', function() {

    it('should route to roleshasusuarios.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'roleshasusuariosCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
