/**
 * Roleshasusuarios model events
 */

'use strict';

import {EventEmitter} from 'events';
var Roleshasusuarios = require('../../sqldb').Roleshasusuarios;
var RoleshasusuariosEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
RoleshasusuariosEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Roleshasusuarios.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    RoleshasusuariosEvents.emit(event + ':' + doc._id, doc);
    RoleshasusuariosEvents.emit(event, doc);
    done(null);
  }
}

export default RoleshasusuariosEvents;
