'use strict';

var app = require('../..');
import request from 'supertest';

var newRoleshasusuarios;

describe('Roleshasusuarios API:', function() {

  describe('GET /api/roleshasusuarios', function() {
    var roleshasusuarioss;

    beforeEach(function(done) {
      request(app)
        .get('/api/roleshasusuarios')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          roleshasusuarioss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      roleshasusuarioss.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/roleshasusuarios', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/roleshasusuarios')
        .send({
          name: 'New Roleshasusuarios',
          info: 'This is the brand new roleshasusuarios!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newRoleshasusuarios = res.body;
          done();
        });
    });

    it('should respond with the newly created roleshasusuarios', function() {
      newRoleshasusuarios.name.should.equal('New Roleshasusuarios');
      newRoleshasusuarios.info.should.equal('This is the brand new roleshasusuarios!!!');
    });

  });

  describe('GET /api/roleshasusuarios/:id', function() {
    var roleshasusuarios;

    beforeEach(function(done) {
      request(app)
        .get('/api/roleshasusuarios/' + newRoleshasusuarios._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          roleshasusuarios = res.body;
          done();
        });
    });

    afterEach(function() {
      roleshasusuarios = {};
    });

    it('should respond with the requested roleshasusuarios', function() {
      roleshasusuarios.name.should.equal('New Roleshasusuarios');
      roleshasusuarios.info.should.equal('This is the brand new roleshasusuarios!!!');
    });

  });

  describe('PUT /api/roleshasusuarios/:id', function() {
    var updatedRoleshasusuarios;

    beforeEach(function(done) {
      request(app)
        .put('/api/roleshasusuarios/' + newRoleshasusuarios._id)
        .send({
          name: 'Updated Roleshasusuarios',
          info: 'This is the updated roleshasusuarios!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedRoleshasusuarios = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedRoleshasusuarios = {};
    });

    it('should respond with the updated roleshasusuarios', function() {
      updatedRoleshasusuarios.name.should.equal('Updated Roleshasusuarios');
      updatedRoleshasusuarios.info.should.equal('This is the updated roleshasusuarios!!!');
    });

  });

  describe('DELETE /api/roleshasusuarios/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/roleshasusuarios/' + newRoleshasusuarios._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when roleshasusuarios does not exist', function(done) {
      request(app)
        .delete('/api/roleshasusuarios/' + newRoleshasusuarios._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
