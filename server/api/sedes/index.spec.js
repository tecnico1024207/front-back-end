'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var sedesCtrlStub = {
  index: 'sedesCtrl.index',
  show: 'sedesCtrl.show',
  create: 'sedesCtrl.create',
  update: 'sedesCtrl.update',
  destroy: 'sedesCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var sedesIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './sedes.controller': sedesCtrlStub
});

describe('Sedes API Router:', function() {

  it('should return an express router instance', function() {
    sedesIndex.should.equal(routerStub);
  });

  describe('GET /api/sedes', function() {

    it('should route to sedes.controller.index', function() {
      routerStub.get
        .withArgs('/', 'sedesCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/sedes/:id', function() {

    it('should route to sedes.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'sedesCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/sedes', function() {

    it('should route to sedes.controller.create', function() {
      routerStub.post
        .withArgs('/', 'sedesCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/sedes/:id', function() {

    it('should route to sedes.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'sedesCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/sedes/:id', function() {

    it('should route to sedes.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'sedesCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/sedes/:id', function() {

    it('should route to sedes.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'sedesCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
