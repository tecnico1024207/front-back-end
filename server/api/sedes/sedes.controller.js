/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/sedes              ->  index
 * POST    /api/sedes              ->  create
 * GET     /api/sedes/:id          ->  show
 * PUT     /api/sedes/:id          ->  update
 * DELETE  /api/sedes/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {Sedes} from '../../sqldb';
import {Empresas} from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Sedess
export function index(req, res) {
  return Sedes.findAll(
    {
      include:[
        {
          model: Empresas
        }
      ]
    }
  )
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Sedes from the DB
export function show(req, res) {
  return Sedes.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Sedes in the DB
export function create(req, res) {
  return Sedes.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Sedes in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Sedes.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Sedes from the DB
export function destroy(req, res) {
  return Sedes.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
