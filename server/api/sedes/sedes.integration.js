'use strict';

var app = require('../..');
import request from 'supertest';

var newSedes;

describe('Sedes API:', function() {

  describe('GET /api/sedes', function() {
    var sedess;

    beforeEach(function(done) {
      request(app)
        .get('/api/sedes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          sedess = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      sedess.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/sedes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/sedes')
        .send({
          name: 'New Sedes',
          info: 'This is the brand new sedes!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newSedes = res.body;
          done();
        });
    });

    it('should respond with the newly created sedes', function() {
      newSedes.name.should.equal('New Sedes');
      newSedes.info.should.equal('This is the brand new sedes!!!');
    });

  });

  describe('GET /api/sedes/:id', function() {
    var sedes;

    beforeEach(function(done) {
      request(app)
        .get('/api/sedes/' + newSedes._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          sedes = res.body;
          done();
        });
    });

    afterEach(function() {
      sedes = {};
    });

    it('should respond with the requested sedes', function() {
      sedes.name.should.equal('New Sedes');
      sedes.info.should.equal('This is the brand new sedes!!!');
    });

  });

  describe('PUT /api/sedes/:id', function() {
    var updatedSedes;

    beforeEach(function(done) {
      request(app)
        .put('/api/sedes/' + newSedes._id)
        .send({
          name: 'Updated Sedes',
          info: 'This is the updated sedes!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedSedes = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSedes = {};
    });

    it('should respond with the updated sedes', function() {
      updatedSedes.name.should.equal('Updated Sedes');
      updatedSedes.info.should.equal('This is the updated sedes!!!');
    });

  });

  describe('DELETE /api/sedes/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/sedes/' + newSedes._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when sedes does not exist', function(done) {
      request(app)
        .delete('/api/sedes/' + newSedes._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
