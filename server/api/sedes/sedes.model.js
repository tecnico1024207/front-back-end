'use strict';

export default function(sequelize, DataTypes) {
  var Sedes= sequelize.define('sedes', {
    id_sede: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: true
    },
    telefono: {
      type: DataTypes.STRING,
      allowNull: true
    },
    direccion: {
      type: DataTypes.STRING,
      allowNull: true
    }
  },
  {
    classMethods:{
      associate: function(models){
        Sedes.belongsTo(models.Empresas,
          {
            onDelete:'CASCADE',
            foreignKey:{
              name:'idEmpresa',
              field: 'id_empresa',
              allowNull:false
            }
          });
      }
    }
  },
{
  underscored:true
});

return Sedes;
}
