'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var tiposEquiposCtrlStub = {
  index: 'tiposEquiposCtrl.index',
  show: 'tiposEquiposCtrl.show',
  create: 'tiposEquiposCtrl.create',
  update: 'tiposEquiposCtrl.update',
  destroy: 'tiposEquiposCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var tiposEquiposIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './tiposEquipos.controller': tiposEquiposCtrlStub
});

describe('TiposEquipos API Router:', function() {

  it('should return an express router instance', function() {
    tiposEquiposIndex.should.equal(routerStub);
  });

  describe('GET /api/tiposEquipos', function() {

    it('should route to tiposEquipos.controller.index', function() {
      routerStub.get
        .withArgs('/', 'tiposEquiposCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/tiposEquipos/:id', function() {

    it('should route to tiposEquipos.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'tiposEquiposCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/tiposEquipos', function() {

    it('should route to tiposEquipos.controller.create', function() {
      routerStub.post
        .withArgs('/', 'tiposEquiposCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/tiposEquipos/:id', function() {

    it('should route to tiposEquipos.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'tiposEquiposCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/tiposEquipos/:id', function() {

    it('should route to tiposEquipos.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'tiposEquiposCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/tiposEquipos/:id', function() {

    it('should route to tiposEquipos.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'tiposEquiposCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
