/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/tiposEquipos              ->  index
 * POST    /api/tiposEquipos              ->  create
 * GET     /api/tiposEquipos/:id          ->  show
 * PUT     /api/tiposEquipos/:id          ->  update
 * DELETE  /api/tiposEquipos/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {TiposEquipos} from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of TiposEquiposs
export function index(req, res) {
  return TiposEquipos.findAll()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single TiposEquipos from the DB
export function show(req, res) {
  return TiposEquipos.find({
    where: {
      idTipoEquipo: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new TiposEquipos in the DB
export function create(req, res) {
  return TiposEquipos.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing TiposEquipos in the DB
export function update(req, res) {
  if (req.body.idTipoEquipo) {
    delete req.body.idTipoEquipo;
  }
  return TiposEquipos.find({
    where: {
      idTipoEquipo: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a TiposEquipos from the DB
export function destroy(req, res) {
  return TiposEquipos.find({
    where: {
      idTipoEquipo: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
