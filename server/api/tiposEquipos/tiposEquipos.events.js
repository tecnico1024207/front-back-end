/**
 * TiposEquipos model events
 */

'use strict';

import {EventEmitter} from 'events';
var TiposEquipos = require('../../sqldb').TiposEquipos;
var TiposEquiposEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TiposEquiposEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  TiposEquipos.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    TiposEquiposEvents.emit(event + ':' + doc._id, doc);
    TiposEquiposEvents.emit(event, doc);
    done(null);
  }
}

export default TiposEquiposEvents;
