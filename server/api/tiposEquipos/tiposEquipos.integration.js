'use strict';

var app = require('../..');
import request from 'supertest';

var newTiposEquipos;

describe('TiposEquipos API:', function() {

  describe('GET /api/tiposEquipos', function() {
    var tiposEquiposs;

    beforeEach(function(done) {
      request(app)
        .get('/api/tiposEquipos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          tiposEquiposs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      tiposEquiposs.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/tiposEquipos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/tiposEquipos')
        .send({
          name: 'New TiposEquipos',
          info: 'This is the brand new tiposEquipos!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newTiposEquipos = res.body;
          done();
        });
    });

    it('should respond with the newly created tiposEquipos', function() {
      newTiposEquipos.name.should.equal('New TiposEquipos');
      newTiposEquipos.info.should.equal('This is the brand new tiposEquipos!!!');
    });

  });

  describe('GET /api/tiposEquipos/:id', function() {
    var tiposEquipos;

    beforeEach(function(done) {
      request(app)
        .get('/api/tiposEquipos/' + newTiposEquipos._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          tiposEquipos = res.body;
          done();
        });
    });

    afterEach(function() {
      tiposEquipos = {};
    });

    it('should respond with the requested tiposEquipos', function() {
      tiposEquipos.name.should.equal('New TiposEquipos');
      tiposEquipos.info.should.equal('This is the brand new tiposEquipos!!!');
    });

  });

  describe('PUT /api/tiposEquipos/:id', function() {
    var updatedTiposEquipos;

    beforeEach(function(done) {
      request(app)
        .put('/api/tiposEquipos/' + newTiposEquipos._id)
        .send({
          name: 'Updated TiposEquipos',
          info: 'This is the updated tiposEquipos!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedTiposEquipos = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTiposEquipos = {};
    });

    it('should respond with the updated tiposEquipos', function() {
      updatedTiposEquipos.name.should.equal('Updated TiposEquipos');
      updatedTiposEquipos.info.should.equal('This is the updated tiposEquipos!!!');
    });

  });

  describe('DELETE /api/tiposEquipos/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/tiposEquipos/' + newTiposEquipos._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when tiposEquipos does not exist', function(done) {
      request(app)
        .delete('/api/tiposEquipos/' + newTiposEquipos._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
