'use strict';

export default function(sequelize, DataTypes) {
  var TiposEquipos = sequelize.define('tipos_equipos', {
    idTipoEquipo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id_tipo_equipo'
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false
    }
    });
  return TiposEquipos;
}
