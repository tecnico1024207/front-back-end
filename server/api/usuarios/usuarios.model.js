'use strict';

export default function(sequelize, DataTypes) {
    var Usuarios = sequelize.define('usuarios', {
      idUsuario: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'id_usuario'
      },
      identificacion: {
        type: DataTypes.STRING,
        allowNull: false
      },
      nombre: {
        type: DataTypes.STRING,
        allowNull: false
      },
      apellido: {
        type: DataTypes.STRING,
        allowNull: false
      },
      direccion: {
        type: DataTypes.STRING,
        allowNull: false
      },
      telefono: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      id_sede_trabajo: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        references: {
          model: 'sedes',
          key: 'id_sede'
        }
      }
    },
    {
        classMethods: {
            associate: function(models){
                Usuarios.belongsToMany(models.Roles,{
                    through: 'roles_has_usuarios',
                    foreignKey:{
                        name:'idUsuario',
                        field: 'id_usuario'
                    },
                    otherKey: {
                        name: 'idRol',
                        field: 'id_rol'
                    }
                });
            }
        }
    },
    {
      underscored: true
    });
    return Usuarios;
  }
