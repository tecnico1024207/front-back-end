/**
 * Sequelize initialization module
 */

'use strict';

import path from 'path';
import config from '../config/environment';
import Sequelize from 'sequelize';

var db = {
  Sequelize,
  sequelize: new Sequelize(config.sequelize.uri, config.sequelize.options)
};

// Insert models below
db.Equipos = db.sequelize.import('../api/equipos/equipos.model');
db.Usuarios = db.sequelize.import('../api/usuarios/usuarios.model');
db.Roleshasusuarios = db.sequelize.import('../api/roleshasusuarios/roleshasusuarios.model');
db.Roles = db.sequelize.import('../api/roles/roles.model');
db.Sedes = db.sequelize.import('../api/sedes/sedes.model');
db.TiposEquipos = db.sequelize.import('../api/tiposEquipos/tiposEquipos.model');
db.Marcas = db.sequelize.import('../api/marcas/marcas.model');
db.Empresas = db.sequelize.import('../api/empresas/empresas.model');
db.Thing = db.sequelize.import('../api/thing/thing.model');
db.User = db.sequelize.import('../api/user/user.model');

Object.keys(db).forEach(function(modelName){
  if("associate" in db[modelName]){
    db[modelName].associate(db);
  }
});

module.exports = db;
